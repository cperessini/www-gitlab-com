---
layout: markdown_page
title: Team Structure
page_class: org-structure
---

GitLab Inc. has at most four layers in the team structure:

1. CEO
1. Executives (e-team) consisting of CxO's and VP's
1. Leads
1. Individual contributors (IC's), which can be a [specialist](/jobs/specialist/) in one things and be an [expert](/jobs/expert/) in multiple things.

The indentation below reflects the reporting relations.
You can see our complete team and who reports to who on the [team page](https://about.gitlab.com/team/).
If there is a hyphen (-) in a line the part before hyphen is the name of the department and sometimes links to the relevant part of the [handbook](https://about.gitlab.com/handbook/).
The job titles link to the job descriptions.
Our open vacancies are at our [jobs page](https://about.gitlab.com/jobs/).


- [General](/handbook/) - [CEO](/jobs/chief-executive-officer/) - Sid Sijbrandij
  - [Marketing](/handbook/marketing/) - [CMO](/jobs/chief-marketing-officer/) - Ashley Smith
    - Design - [Designer](/jobs/designer/) - Luke Babb
    - [Demand generation](/handbook/marketing/demand-generation) - [Senior Demand Generation Manager](/jobs/demand-generation-manager/) (lead) - Hank Taylor
      - [Online Marketing](/handbook/marketing/online-marketing) - [Online Marketing Manager](/jobs/online-marketing-manager/) - Mitchell Wright
      - Business Development - [Business Development Team Lead](/jobs/business-development-team-lead/) - Colton Taylor
      - [Business Development Representatives](/jobs/business-development-representative/)
    - [Product Marketing](/handbook/marketing/product-marketing/) - [Senior Product Marketing Manager](/jobs/product-marketing-manager/) (lead) - Amara Nwaigwe
      - [Partner Marketing](/handbook/marketing/product-marketing/#partnermarketing/) - Partner/Channel Marketing Manager (vacancy)
      - [Content Marketing](/handbook/marketing/developer-relations/content-marketing/) - Content Marketing Manager (vacancy)
    - Developer Relations
      - [Developer Advocacy](/handbook/marketing/developer-relations/developer-advocacy/) - [Developer Advocate](/jobs/developer-advocate/)
      - [Field Marketing](/handbook/marketing/developer-relations/field-marketing/) - [Field Marketing Manager](/jobs/field-marketing-manager/) - Emily Kyle
      - Technical Writing - [Technical Writer](/jobs/technical-writer/)
  - [Sales](/handbook/sales/) - [CRO](/jobs/chief-revenue-officer/) - Chad Malchow
    - Americas Sales - [Account Executive](/jobs/account-executive/)
    - EMEA Sales - [Sales Director EMEA](/jobs/sales-director/) (lead) - Richard Pidgeon
      - [Account Executive](/jobs/account-executive/)
    - APAC Sales - [Director of Global Alliances and APAC Sales](/jobs/director-of-global-alliances-and-apac-sales/) (lead) - Michael Alessio
    - Customer Success - Customer Success Manager (lead, future vacancy)
      - [Account Manager](/jobs/account-manager/)
      - [Success Engineer](/jobs/success-engineer/)
  - [People Operations](/handbook/people-operations/) - [VP of People Operations](/jobs/vp-of-people-ops/) (lead, vacancy)
    - [People Operations Coordinator](/jobs/people-ops-coordinator/)
  - Finance - [CFO](/jobs/chief-financial-officer/) - Paul Machle
    - [Accounting](/handbook/accounting/) - [Controller](/jobs/controller/) - Wilson Lau
  - [Technical Direction](/direction/) - [CTO](/jobs/chief-technology-officer/) - Dmitriy Zaporozhets
  - Engineering - [VP of Engineering](/jobs/vp-of-engineering/) - Stan Hu
    - Backend - [Backend Lead](/jobs/backend-lead/) - Douwe Maan
      - [Developers](/jobs/developer/) that are backend specialists
    - Frontend - [Frontend Lead](/jobs/frontend-lead/) - Jacob Schatz
      - [Frontend Engineers](/jobs/frontend-engineer/)
      - [UX Designers](/jobs/ux-designer/)
    - [Infrastructure](/handbook/infrastructure/) - Infrastructure lead - Pablo Carranza
      - [Production Engineers](/jobs/production-engineer/)
      - [Developers](/jobs/developer/) that are a performance specialist
    - Packaging - Packaging lead - Marin Jankovski
    - [Support](/handbook/support/) - Support lead - José Torres
      - [Service Engineers](/jobs/service-engineer/)
    - [Developers](/jobs/developer/) that are maintainers
    of or specialist in projects without a lead
    - [Developers](/jobs/developer/) that are a merge request coach
  - Scaling - [VP of Scaling](/jobs/vp-of-scaling/) - Ernst van Nierop
  - General Product - [VP of Product](/jobs/vice-president-of-product/) - Job van der Voort
  - CI/CD Product - [Head of Product](/jobs/head-of-product/) - Mark Pundsack
