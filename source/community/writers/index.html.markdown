---
layout: markdown_page
title: "Write for GitLab"
comments: false
sharing: true
suppress_header: true
---
![Write for GitLab](/images/community/computers-table-banner.jpg)

# Write for GitLab

We’re opening our blog up to contributions from the community.

When you get published on our site you can earn $50 to $200 for technical articles. If you’re accepted, you’ll get feedback on your writing, and be guided through making the best resources. Find out how to get started!

All great contributions come from developers “scratching their own itch.” It’s likely you can share some advice to help someone along the way. You can contribute the technical content you wish was available.

### How it works

1.  Submit topics and a writing sample to us.
2.  Get approved. Sign contracts.
3.  Start writing, get feedback and revise.
4.  Publish.
5.  Get paid.

First you’ll submit a writing sample and tell us about your areas of expertise. After you get approved, we’ll contact you to initiate the writing process. You’ll get detailed feedback on your writing as we push the article to publication. Once it’s published you’ll get paid.

In terms of pricing, we looked to the industry to see what were the going rates. Your feedback is welcome. Here are our rough guidelines for rates. However, we keep an [open list of topics](https://gitlab.com/gitlab-com/blog-posts/issues?milestone_id=&scope=all&sort=created_desc&state=opened&utf8=%E2%9C%93&assignee_id=0&author_id=&milestone_title=&label_name=&weight=) we have bounties on. That can help you identify our most high-priority topics.

- Brief "Quick tips" or feature highlights of less than 800 words. - $50
- Short tutorials of 800-1500 words. - $100
- In-depth tutorials or opinion pieces of 1,500+ words. - $200

### What we're looking for

We’re inviting community contribution so we can expand the range of tutorials and advice about creating, collaborating and deploying with GitLab.

It's important that the content is:

- Accurate
- Complete
- Original

We'll be able to develop a more complete style guide as we grow out the program. Meanwhile we will rely on referencing [the Digital Ocean style guide, which inspired our community writers program](https://www.digitalocean.com/community/tutorials/how-to-write-an-article-for-the-digitalocean-community).

To find out what topics we're looking for, review [the blog post backlog](https://gitlab.com/gitlab-com/blog-posts/issues?milestone_id=&scope=all&sort=created_desc&state=opened&utf8=%E2%9C%93&assignee_id=0&author_id=&milestone_title=&label_name=&weight=) and see if there are any existing requests for topics that inspire you.

Review our [Blog Post Style Guide](https://gitlab.com/gitlab-com/blog-posts/blob/master/STYLEGUIDE.md)

### Example topic areas

- Comparison posts; Git v W; GitLab v X; GitLab CI v Y. GitLab EE v Z.
Migrating from X to Git and/or GitLab.
- Working with GitLab, feature highlights and tutorials.
- Ways of boosting efficiency.
- Extending capability with integrations.
- Improving communication in code-collaboration.
- Managing open source projects.
- Testing X with GitLab CI

Got an idea?

[Submit your proposal!](http://ow.ly/WWMAg)

### Find out more

- [Our blog](http://about.gitlab.com)
- [Blog post backlog](http://ow.ly/WWA2n)
- [Submit your proposal](http://ow.ly/WWMAg)
- [Blog Style Guide](https://gitlab.com/gitlab-com/blog-posts/blob/master/STYLEGUIDE.md)
