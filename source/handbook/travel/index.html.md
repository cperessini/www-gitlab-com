---
layout: markdown_page
title: "Travel"
---

## Arranging your company travel

The company will cover all work related travel costs.  
- Understand that the guidelines of [Spending Company Money](https://about.gitlab.com/handbook/#spending-company-money) still apply
- The company can accommodate your custom request. It is OK to stay longer on
your trip. However, the extra days will not be covered by the company.

## Specific Topics Relating to Egencia and Expenses

- [Arranging a visa for travel](#arrange-visa)
- [Tips and Tricks for visa arrangements](#visa-tips)
- [Setting up your Egencia account](#setup-egencia)  
- [Booking travel through Egencia](#egencia)  
- [Expenses while traveling](#expenses-while-traveling)


### Arranging a visa for travel <a name="arrange-visa"></a>
In some cases when traveling to a conference or summit you might need to arrange a visa to enter that specific country.
Always check the website of the Embassy of the Country you want to visit located in your home country. They will have the most updated information on wether or not you need a visa and what is needed for your application. In some instances you can travel under a visa waiver program (e.g. ESTA) in other you might need to arrange a tourist or business visa. Read their website carefully and ask People Ops if you need any help with acquiring the needed documents for your application.

#### Tips & Tricks for visa arrangements <a name="visa-tips"></a>
- Make sure to start in time with your arrangements for a visa. It can take up to 3-4 months to acquire a visa.
- Check if your passport is still valid long enough
- Give priority to arranging your visa, it can take time to get an appointment to apply at the Embassy.
- Double check if you have all needed document and information. Ask People Ops to help you check this if you like.
- Think of the answers you will give during your visa interview; it can influence your visa approval/denial.
- Once the dates for a conference or summit are set start asap with your application if you have needed a special visa in previous situations, just to give yourself enough time and room for errors if they arrise.

### Setting up your Egencia account<a name="setup-egencia"></a>

Below is some more information to help you get set up with your [Egencia](https://www.egencia.com) account.  

1. You will receive an email from Egencia with your username.
1. With the link, from step one in this email, you can set a new secure password.
1. Once you are in your dashboard, setup your info in your profile (see link at top right corner of page).
1. Make sure to add your phone number in all three "phone number" fields (it can be the same number).
1. Add your passport information to your profile (only passport information is accepted in Egencia at this time), this will populate your info directly when booking a flight.
1. Now let's start booking!

### Booking travel through Egencia <a name="egencia"></a>

If you book through [Egencia](https://www.egencia.com), the costs will be added
to the GitLab invoice and **no credit card is needed**.

1. Please note that some budget airlines might not show in Egencia, so make sure
to check those to be sure there is no better option out there. (SouthWest, JetBlue, etc.).
1. Login to your account. If you've not yet set up your account, do this [first](#setup-egencia).
1. The dashboard gives you the option to start a booking directly. The options are
to book flights, train tickets, hotels, or rental cars.
1. Input info for the flight you want to book.
1. Select your favorite option for the outbound flight. Please note that our policy
is that you can book up to $250 over the cheapest flight option. Booking more than
$250 over that amount will flag "out of policy" and requires approval.
1. Select your return (inbound) flight.
1. Read & check the box "I have read and accept the rules and restrictions". Now
you have the possibility to "continue to checkout" or "add more items to this trip"
such as another flight, a hotel reservation or booking a car.
1. Select "continue to checkout" if you just want to book the flight.

#### Checkout flight only

1. Enter your passport information in the "Secure flight" window. This will be
pre-populated if you setup your profile correctly. At this point, you do have the
option of using your US drivers license for domestic flights within the US.
1. Enter home, work, and mobile phone number (they can be the same number). This
will be pre-populated if you already set up your profile before.
1. Enter your seat preferences and frequent flyer program info if wanted.
1. Enter your reason for booking, this is required for every booking.

#### Continue adding a hotel or car

Under "add more items to this trip" you can select what you want to add.

1. The platform auto populates your info based on your flight.
1. To add a **hotel**:
  - You will get a window including a map with all hotel options.
  - On the top right you can filter based upon preferences
  - Click on a hotel to get more information
  - To go back to the overview of hotels click on X in the top right corner
  - To choose the hotel room click on the price
  - When choosing the hotel you will go to an overview of the costs and hotel info
1. To add a **car**:
  - You will get a window with your car options
  - On the top of the window you can sort on price
  - To select a car click "select"
  - When choosing the car you will go to an overview of the costs and rules/restrictions info
Now you once again have the possibility to "continue to checkout" or "add more items to this trip" such as another flight

#### Checkout flight and hotel and/or a car

You will get a total overview of what is booked. You can see that the hotel and/or car are awaiting confirmation of the third party that will be booked once you checkout.

#### Options in Egencia

* Instead of choosing "checkout" you can select "save trip and view itinerary"
* Here you will get an overview of everything that was selected and you can change items, such as your flight.
* In the menu on the top left you have some options like "invite others", "print itinerary", send, or delete.
* You can also download the app "Egencia TripNavigator" to keep your itinerary close and even do bookings on the go if needed.

Please send a message to travel@gitlab.com if you need help in booking your travel.

#### Booking shows Out of Policy

* Sometimes there is no way around booking a flight or hotel that is out of policy.
* This means that the option selected is more expensive than the cheapest option + our buffer.
* When you try to book this, an email will be sent to travel@gitlab.com for approval.
* Please provide extensive reasoning to make approval easier and faster.
* After 24 hours without any manual action (approved/denied) the booking will automatically be accepted.

### Expenses While Traveling <a name="expenses-while-traveling"></a>

1. The company will pay for lodging and meals during the part of the trip
that is work-related. Depending on the distance of your travel, this can include
one day before and one day after the work related business. For example, if you
are attending a 3 day conference in a jetlag-inducing location, the company will
cover your lodging and meals those 3 days as well as one day before and one day after.
1. Meals and lodging are covered. The "spend it like it is your own money" rule applies here except
if explicitly noted otherwise. For example, the limits for during the Austin Summit
are detailed on the [Austin Summit page](https://dev.gitlab.org/summit_group_2016/Austin-Summit-2016-project).
1. Always bring a credit card with you when traveling for company business if you have one.
1. Hotels will generally expect you to have a physical credit card to present upon check-in.
This credit card will be kept on file for the duration of your stay. Even if your lodging
was pre-paid by the company or by using a company credit card, the Hotel may still
require a card to be on file for "incidentals".
1. If you incur any work-travel related expenses (on your personal card or a GitLab
company card), please make sure to save the original receipt.
1. When your trip is complete, please file an expense report via Expensify.
Expensify steps can be found http://help.expensify.com/getting-started/

See the section on [Spending Company Money](https://about.gitlab.com/handbook/#spending-company-money)
on the handbook for more details on the expense policies.
