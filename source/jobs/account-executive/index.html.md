---
layout: markdown_page
title: "Account Executive"
---


## Responsibilities

* Account Executive (AE) will report to the CRO or a regional director over the course of the year
* Act as a primary point of contact and the face of GitLab for our prospects.
* Contribute to post-mortem analysis on wins/losses.
   * Communicate lessons learned to the team, including account managers, the marketing team, and the technical team.
* Take ownership of your book of business
    * document the buying criteria
    * document the buying process
    * document next steps and owners
    * ensure pipeline accuracy based on evidence and not hope
* Contribute to documenting improvements in our [sales handbook](https://about.gitlab.com/handbook/sales/).
* Provide account leadership and direction in the pre- and post-sales process
* Be the voice of the customer by contributing product ideas to our public [issue tracker](https://gitlab.com/gitlab-org/gitlab-ee/issues)        

## Requirements

* A true desire to see customers benefit from the investment they make with you
* 2+ years of experience with B2B sales
* Interest in GitLab, and open source software
* Ability to leverage established relationships and proven sales techniques for success
* Effective communicator, strong interpersonal skills
* Motivated, driven and results oriented
* Excellent negotiation, presentation and closing skills
* Preferred experience with Git, Software Development Tools, Application Lifecycle Management

Work remotely from anywhere in the world. Curious to see what that looks like? Check out our [remote manifesto]( https://about.gitlab.com/2015/04/08/the-remote-manifesto/)!
