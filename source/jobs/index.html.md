---
layout: markdown_page
title: "Jobs"
---

## About GitLab

GitLab is quickly becoming the standard for source code version
management for any company, large or small. At GitLab, we help large
enterprises move to Git and build better software.

## Why work for GitLab?

GitLab is growing fast.
Working for GitLab means joining a very productive, ambitious team, where independence
and flexibility are both valued and required.
Your work will directly have a large impact on the present and future of GitLab.
We like to spend our time on things that matter.
We are a [remote only company](/2015/04/08/the-remote-manifesto/)
and you can work from wherever you want.
For more background please see our [about page](/about/),
our [culture page](/culture/), and our [handbook](/handbook/).

If you see yourself as a good fit with our company’s goals and team, then please
review the current job openings on this page, and submit your resume and cover
letter to be considered!

<iframe width="560" height="315" src="https://www.youtube.com/embed/GJP-3BNyCXw" frameborder="0" allowfullscreen></iframe>

## We don't work with recruiters<a name="no-recruiters"></a>

We do not accept solicitations by recruiters, recruiting agencies, headhunters and outsourcing organizations.
If you email us we'll reply with [a link to this paragraph](/jobs/#no-recruiters) to indicate we would very much appreciate it if you stop emailing us and remove us from any marketing list.

## Available Openings

### Account Executive - Sales

- [Description](/jobs/account-executive/)
- [Apply](https://gitlab.workable.com/jobs/88120/candidates/new)

### Account Manager - Customer Success

- [Description](/jobs/account-manager/)
- [Apply](https://gitlab.workable.com/jobs/242362/candidates/new)

### Content Marketing Manager

- [Description](/jobs/content-marketing-manager/)
- [Apply](https://gitlab.workable.com/jobs/252104/candidates/new)

### Director Customer Success

- [Description](/jobs/dir-customer-success/)
- [Apply](https://gitlab.workable.com/j/566AD7D3C8)

### Front-End Engineer

- [Description](/jobs/frontend-engineer/)
- [Apply](https://gitlab.workable.com/jobs/181461/candidates/new)

### General Ledger Accountant (part time)

- [Description](/jobs/general-ledger-accountant/)
- [Apply](https://gitlab.workable.com/jobs/260762/candidates/new)

### People Operations Coordinator

- [Description](/jobs/people-ops-coordinator/)
- [Apply](https://gitlab.workable.com/jobs/220106/candidates/new)

### VP of People Operations

- [Description](/jobs/vp-of-people-ops/)
- [Apply](https://gitlab.workable.com/jobs/234625/candidates/new)

### Production Engineer

- [Description](/jobs/production-engineer/)
- [Apply](https://gitlab.workable.com/jobs/142989/candidates/new)

### Sales Operations Manager

- [Description](/jobs/sales-operations-manager/)
- [Apply](https://gitlab.workable.com/jobs/236037/candidates/new)

### Service Engineer

- [Description](/jobs/service-engineer/)
- [Apply](https://gitlab.workable.com/jobs/87722/candidates/new)

### Success Engineer (Customer Success)

- [Description](/jobs/success-engineer/)
- [Apply](https://gitlab.workable.com/j/6A9FA4A8DE)


### UX Designer

- [Description](/jobs/ux-designer/)
- [Apply](https://gitlab.workable.com/jobs/227708/candidates/new)
