---
layout: markdown_page
title: "Executive Assistant"
---

## Responsibilities

* Handle email in executive's name including triage.
* Schedule meetings if Calendly doesn't work.
* Prepare draft responses to emails based on experience or verbal instructions.
* Make changes to the handbook based on verbal instructions.
* Ensure executive's appointments are confirmed a day in advance and that the attendees have access to and are aware of the relevant materials (about.gitlab.com/primer, google doc, etc.).
* Research applicants.
* Do screening calls with applicants.
* Draft contracts.
* Take care of expenses.
* Stage contracts in our software.
* Take care of contract paperwork.
* Take care of travel, lodging, and lunch/dinner reservations.
* Sign up for services, make changes to them, and close them.
* Coordinate internal and external events, summits, conferences and off-sites.
* Suggest more efficient processes and procedures.
* Help out with Dutch translations and interactions.
* Handle paperwork for immigration related tasks.
* Office management related tasks.
* Get a lot of feedback on how to handle above tasks and encode them in the handbook.
