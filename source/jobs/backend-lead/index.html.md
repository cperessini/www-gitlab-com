---
layout: markdown_page
title: "Backend Lead"
---

## Responsibilities

* Interviews applicants for development positions
* Continues to spend part of time coding
* Indicates to the VP of Product when a release is full, leaving room for bugfixes and other urgent tasks that will still arise
* Ensures that the technical decisions and process set by the VP of Engineering and CTO are followed
* Ensures that there is a written onboarding and process that is effective and followed
* Does the release kickoff
* Does the release retrospective
* Does 1:1's with all reports every 2-5 weeks (depending on the experience of the report)
* Is available for 1:1's on demand of the report
* Uses the contributor analytics to ensure that people that are stuck are helped
* Ensures all tentpole features, promised and bugs are assigned early in the release
* Ensures that all issues that can't be shipped are moved quickly
* Reviews a significant amount of the merge requests made by developers
* Delivers input on promotions, function changes, demotions and firings in consultation with the CEO, CTO, and VP of Engineering
